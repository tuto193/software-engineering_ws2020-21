| Gewünscht | `git` | `svn` |
|-----|:---:|----:|
| Anlegen eines neuen, leeren Quell-Repositories | `git init` | `svn admin create` |
| Kopieren in ein neues Arbeitsverzeichnis| `cd .. && mv [folder] /path/to/move/to`  | `svn cp QUELLPFAD ZIELPFAD` |
| Aktualisierung eines bestehenden Arbeitsverzeichnisses | `git add . && git commit` | `svn update` |
| Hinzufügen von Dateien und Verzeichnissen in die Versionskontrolle | `git add` | `svn add` |
| Rückgängig machen von lokalen Änderungen | `git reset` | `svn revert` |
| Übertragen von Änderungen in das Quell-Repository | `git commit && git push` | `svn commit` |
| Verschieben, Löschen und Umbenennen von Dateien und Verzeichnissen | `git mv / git rm` | `svn mv / svn rm / svn del` |
| Erzeugen von Branches | `git branch` | `nicht implementiert` |
| Erzeugen von Tags | `git tag` | `nicht implementiert` |
| Zusammenführen eines Branches mit dem Hauptentwicklungspfad mit unterschiedlichen Än-derungen in derselben Datei | (während im **master**) `git merge MeinBranch` | `nicht implementiert` |
