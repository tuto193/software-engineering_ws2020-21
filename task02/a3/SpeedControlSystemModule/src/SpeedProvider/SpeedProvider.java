package SpeedProvider;

public interface SpeedProvider {
    public int getTargetSpeed();

    public String getDescription();
}
