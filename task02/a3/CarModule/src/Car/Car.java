package Car;

import java.util.ServiceLoader;
import java.lang.System;

import SpeedProvider.SpeedProvider;

public class Car {

    private static void printHelp() {
        System.out.print("Options are:\n\t-d\tRun with dynamic cruise control\n\t-a\tRun with automatic cruise control\n");
    }

    public static void main(String[] args) {
        // Check amount of arguments
        if(args.length < 2) {
            System.out.print("Expected an arguments.\n");
            printHelp();
            return;
        }
        else if(args.length > 2) {
            System.out.print("Too many arguments inserted.\n");
            printHelp();
        }

        // Check for the correctness of the parameters
        switch(args[1]) {
            case "-d":
                break;
            case "-a":
                break;
            default:
                System.out.print("Argument not accepted.\n");
                printHelp();
                return;
        }
        // Das hier macht keinen Sinn, aber das Beispiel ist so
        SpeedProvider x = ServiceLoader.load(SpeedProvider.class);

    }
}
