package cPackage;

import java.util.ServiceLoader;

import aPackage.AInterface;

public class CClass {
	
	public static void main(String[] args) {
		
//		AInterface a = new BClass();
		Iterable<AInterface> providers = ServiceLoader.load(AInterface.class);
		for(AInterface a: providers) {
			a.printHelloWorld();
		}
	
	}
}
