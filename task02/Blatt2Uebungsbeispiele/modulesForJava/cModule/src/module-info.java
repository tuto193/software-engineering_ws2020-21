/**
 * @author Lars Huning
 *
 */
module cModule {
	
	//Da wir ServiceLoader verwenden, reicht es aus wenn wir aModule kennen
	//Achtung: aModule muss zum buildPath hinzugefuegt werden
	requires aModule;
	
	//Explizite Angabe, dass wir das Interface nutzen. Ohne diese Angabe
	//funktioniert ServiceLoader nicht (weil er sonst auf Implementierungsdetails basieren wuerde,
	//statt auf der Spezifikation)
	uses aPackage.AInterface;
}