/**
 * @author Lars Huning
 *
 */
module bModule {
	
	//Zugriff auf alle mit "exports" gekennzeichneten Packages in aModule
	//Achtung! "requires" beschreibt nur die Dependency. Das Modul muss noch dem
	//Module Path hinzugefeugt werden
	requires aModule;
	
	//Alle Klassen in dem Package sind von aussen zugreifbar
	exports bPackage;
	
	//Wir teilen anderen Modulen mit, dass wir ein bestimmtes Interface erfuellen
	//Das erlaubt diesen anderen Modulen unsere Realisierung dynamisch zu finden,
	//ohne ihren Namen zu kennen
	provides aPackage.AInterface with bPackage.BClass;
}