# A1
* Wenn er schlëchter geplant hätte, hätte der Graph eine Tendez, nach rechts zu liegen/lehnen.
* Wenn er besser geplant hätte wäre diese Tendez komplett gerade, oder es hätte sogar eine Tendenz, nach Links zu liegen/lehnen.
* Verzögerungen sind an den Meilensteinen daran zu erkennen, dass ihr voraussichtliches Ende nach Rechts gezogen wird.

# A2
Zeit = 5 Wochen

Mitarbeiter = $n$ (am Anfang $n = 3$)

Entwicklungsaufwand = 600h $|n \cdot 40h/Woche \cdot 5Woche$

$\text{Kommunikationsaufwand } k = c_k{n \choose 2 } \simeq c_k \cdot \frac{n^2}{2}\\
\text{mit }n = 3 \\
\text{und } c_k = 2$

$\Rightarrow k = 2\cdot \frac{3^2}{2} = 3^2$

Aufwand pro $n=3$ Programmierer
$\\
f(3) \coloneqq 200h = \frac{600}{n}
$,

$\Rightarrow f(n) = \frac{600}{n · 5 · 40} =$

Dann ist die Entwicklungsdauer am Niedrigsten, wenn $f(n) = k$

$\Rightarrow  r(n) =k + f(n) \Rightarrow \frac{600}{n} + n^2 \\
r(n) = \frac{600}{n} + n^2 \\
r'(n) = \frac{-600}{n^2} + n^2
r'(n) = 0 \\
\Rightarrow \frac{-600}{n^2} + 2n = 0 \\
2n = \frac{600}{n^2} \\
n^3 = \frac{600}{2} \\
n = \sqrt[3]{300} \\
n \simeq 6.69 \simeq 7 \\
r(7) = \frac{600}{7} + 7^2 = 85.71 + 42 = 134.71
$

Das sind 85.71 Stunden pro Mitarbeiter über 3.367 Wochen. Das sind fast 15 Tage weniger als vorher.
